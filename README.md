# SSRLoS

## Git

目前代码还在整理中，稍后会开源。

### Cocos2dx v3.17.2

#### Source

https://gitee.com/supersuraccoon/ssrlos-cocos2dx

#### Demo

http://supersuraccoon.gitee.io/ssrlos-cocos2dx

### Cocos Creator v1.10.0

#### Source

https://gitee.com/supersuraccoon/ssrlos-cocoscreator_v1

#### Demo

http://supersuraccoon.gitee.io/ssrlos-cocoscreator_v1

### Cocos Creator v2.4.0

#### Source

https://gitee.com/supersuraccoon/ssrlos-cocoscreator_v2

#### Demo

http://supersuraccoon.gitee.io/ssrlos-cocoscreator_v2

## Preface / 前言

开始写这个项目差不多是 `2017` 时候的事了，当出这一模块只是一个 `地下城游戏DEMO` 的一部分。编写那个 `DEMO` 的原因主要是为了学习一些在平时工作中不太会接触到的游戏技术和算法。

关于这个 `DEMO` ，可以参考 `2017` 我写的这篇长文: [ccdungeons_dev_log](http://supersuraccoon-cocos2d.com/ccdungeons_dev_log/ccdungeons_dev_log.html) 。

![ccdungeons1](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ccdungeons1.png)

![ccdungeons2](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ccdungeons2.png)

![ccdungeons3](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ccdungeons3.png)



这里的 `LoS` -- `Line Of Sight`，简单来说，想象一个角色在充满障碍物的游戏地图中，哪些部分是角色可见的，哪些部分对于角色是不可见的，也就是角色的 `视线实际可见的范围是哪些`。

与此相关的概念，还有 `FoV` -- `Field of Vision / View`，可以理解为 `视野范围`，而通过 `LoS` 算法，可以最终计算出实际的 `视野范围`。

`LoS`作为其中的一个重要模块，在最初模块只是为了配合 `Tiled Map` 使用，所以在设计和实现上没有什么特别的讲究。但是随着之后对该模块的深入研究和扩展，花费了很多的时间进行了特别的优化和设计。

当然因为都是在业余时间完成的，所以断断续续的就做了很多年，期间修改了无数的 `bug`，优化了无数的细节。

最近看到论坛有人提到 [[建议]2D灯光](https://forum.cocos.org/t/2d/96799/9)，然后在官方的 [trello](https://trello.com/c/Le2zJVOf/264-2d-lighting) 也提到了有类似的计划，其中提到的参考文章也都是我所看过的。因此打算把这些年做的整理下，拿出来开源，然后也写篇文章，记录下一些开发相关的东西，其实有些东西由于时间久远而且资料也有所缺失，自己都已经忘了当初是怎么解决的了，挺后悔当初没有留下一些记录。


## Features / 功能

### ComponentCore

核心算法模块，用于根据给定障碍物的信息，实时计算可见范围的组件。

#### Vision Type / 视野类型

. `Unlimited Range` 不限制视线距离，不限制视线角度: `360°`	

​	由于视线距离无限，相当于视野的边界是一个矩形，因此有着独特的附加属性	

​	.. `Custom Sight Size` 固定视野大小，位置不固定，跟随对象移动

​	.. `Custom Sight Rect` 固定视野大小，位置，不随对象移动而移动

. `Limited Range Full Angle` 限制视线距离，不限制视线角度: `360°`

. `Limited Range Non Reflex Angle`限制视线距离，限制视线角度: `(0°, 180]`

. `Limited Range Reflex Angle`限制视线距离，限制视线角度: `(180°, 360)`

#### Obstacle Type / 障碍类型
. `Segment` 线段

. `Polyline` 折线

. `Concave` 凹多边形

. `Convex` 凸多边形

#### Component Related / 组件相关

. 可以挂载到任意 `cc.Node` 对象上

. 支持挂载节点的形变 -- `Scale` 缩放 / `Rotation` 旋转 / `Skew` 扭曲

. 支持复数组件同时使用

#### Algorithm Output / 算法输出
. `Visible Area` 可视区域多边形信息

. `Potential Blocking Edge` 所有潜在边信息

. `Blocking Edge` 所有碰撞边信息

. `Hit Point` 所有碰撞点信息

. `Visible Edge` 所有照亮边信息

#### 引擎支持

- [x] `cocos2d-x web`
- [x] `cocos2d-x native`
- [x] `Creator v1 web`
- [x] `Creator v1 native`
- [x] `Creator v2 web`
- [x] `Creator v2 native`

### ComponentMask

用于渲染 `ComponentCore` 所计算出的可视范围，实现遮罩效果。

- [x] `cocos2d-x web`
- [x] `cocos2d-x native`
- [x] `Creator v1 web`
- [x] `Creator v1 native`
- [x] `Creator v2 web`
- [ ] `Creator v2 native`

### ComponentShadow

同样用于渲染 `ComponentCore` 所计算出的可视范围，主要用于实现 `2d` 阴影效果。

- [x] `cocos2d-x web`
- [ ] `cocos2d-x native`
- [ ] `Creator v1 web`
- [ ] `Creator v1 native`
- [ ] `Creator v2 web`
- [ ] `Creator v2 native`

### ComponentRender

用于渲染 `ComponentCore` 算法输出的各种信息。

- [x] `cocos2d-x web`
- [x] `cocos2d-x native`
- [x] `Creator v1 web`
- [x] `Creator v1 native`
- [x] `Creator v2 web`
- [ ] `Creator v2 native`

### ComponentLight

用于渲染 `ComponentCore` 所计算出的可视范围，附带光照的效果。

- [x] `cocos2d-x web`
- [x] `cocos2d-x native`
- [x] `Creator v1 web`
- [x] `Creator v1 native`
- [x] `Creator v2 web`
- [ ] `Creator v2 native`

## Engine Support / 引擎支持

`LoS` 最初开发的时候，使用的是 `cocos2d-x v3.12` 版本，后期逐步升级到最新的 `v3.17` 版本。

同时在后期，`Creator` 开始变成主流后，我也开始试着向 `Creator` 进行适配。

移植适配的主要工作内容有:

. 部分 `API` 发生变化

. 算法部分，依赖了 `cc.Node` 对象

. 算法部分，依赖了 `cc.Node` 对象

. 渲染部分主要依赖 `cc.DrawNode` 对象

. 渲染部分有两个特殊的 `Mask` 和 `ShaderLight` 需要特殊处理

. 绑定的实现部分语法不一样

下面是 `LoS` 模块在目前各引擎的实现情况。

### cocos2d-x v3.x

`Web`

最初基于 `cocos2d-x v3.12` 开发，目前适配到最新的 `v3.17` 版本。

前期在 `Web` 端同时支持  `WebGL` 和 `Canvas` 两种渲染模式，现在只支持 `WebGL` 模式。

`Native` 

所有模块都进行了原生 `C++` 的移植，绑定层实现了核心算法模块和渲染模块，数据模块暂时没有实现。

### Creator2D v1.x

`Web`

`Creator2D v1.x` 的设计和 `cocos2d-x` 还是有很多相近的地方，因此 `Web` 层的实现，只是单纯的替换一些 `API`，加上替换一些原有的渲染对象(如 `cc.XXX` 变为 `_ccsg.XXX`)，核心代码几乎完全一致。

为配合 `Creator` 基于组件的开发模式，为核心算法和渲染模块提供了 `Creator` 风格的组件。

对于渲染模块， `Creator v1.x` 同时保留了旧的 `cc.DrawNode` 类和新的 `cc.Graphics` 类，代码中大部分的渲染都使用了新的 `cc.Graphics` 模块。

对于 `Mask` ，`Creator` 底层实际使用的渲染对象是 `cc.DrawNode` ，和 `cocos2dx` 一致。

对于 `ShaderLight`，还是采用重写 `cc.DrawNode` 的做法，和 `cocos2dx` 任然保持一致。

`Native`

模块移植最初在 `Creator2D v1.5` ，那时候绑定层还是 `SpiderMonkey v38` 和 `cocos2d-x` 完全一致 ，因此整个原生到绑定层几乎不需要做任何修改即可。

后期  `Creator2D v1.6` 开始，绑定层升级到了 `SpiderMonkey v52`，绑定 `API` 发生了一些变化，因此需要作出一些针对的修改才能完成绑定。

### Creator2D v2.x

`Web`

 `Creator2D v2.x` 彻底重写了底层渲染实现逻辑， 去除了一部分不用的旧模块，但是在设计理念上和 `v1.x` 还是完全一致的，因此在移植方面，也是可以几乎保持一致，唯一需要作出比较大的修改的是 `ShaderLight`，`Shader` 的实现需要改成 `Material/Effect` 的形式。

`Native`

同样的在 `Creator2D v2.x` ，绑定层同样的是彻底的重写，不再是单一的使用某种绑定技术，而是根据实际运行环境进行动态选择的策略，因此绑定层需要完全的重写。

同样的在原生层 `C++`，大部分的模块的原生实现都已经被删除，如 `cc.Node/cc.Sprite/cc.Label` 等等，只保留了 `NodePorxy` 这样的一个对象，因此 `C++` 部分的代码也需要作出对应的修改。

渲染模块目前没有进行原生的移植，因为暂时还不清楚怎么做。现在的做法是，算法部分完全有原生进行，所有输出的数据，都进行了绑定实现，可以在脚本层进行获取再进行渲染。

### Creator2D v3.x

传说中会进行 `2D/3D` 的代码合并，目前还不清楚会有多大的变化。

### Creator3D v1.x

这里提到 `3D` 版本，并不是指的扩展到 `3D` 空间，而是指原有模块在保持 `2D` 的情况下，在 `Creator3D v1.x` 能否正常使用。

目前进行了简单的尝试，由于  `Creator3D v1.x` 在模块的编写上又有着不同于 `Creator3D v2.x` 的要求 (如使用 `ts` 或是 `es6` 标准进行编码)，目前暂时还没有进行适配。

## Demonstration / 演示

### Main Features / 主要功能

开源的演示程序中包括以下一些内容 (目前 `cocos2dx` 版本以外，仅包含 `Performance Test`):

.  `Testcase`

​	所有视线类型的测试用例

![ssrlos11](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos11.png)

. `Performance Test`

​	性能测试用例

![ssrlos12](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos12.png)

. `Playground`

​	.. 支持所有输出数据的可视化显示

​	.. 支持所有视线类型及属性的动态切换

​	.. 可自由的创建任意的障碍物，光源

​	.. 支持场景的导出导入(**仅限浏览器端**)

![ssrlos13](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos13.png)

### Auxiliary Features / 辅助功能

除了主要功能的演示，还有一些辅助用模块

. `Illumination` / 照明
    配合视线系统，实现简单的光照效果

. `Polygon Editor` / 多边形编辑器
    实时多边形编辑器，可以添加新的多边形对象，支持拖拽，变形

. `Drawing Editor` / 手绘编辑器

. `Floating Menu` / 浮动菜单栏

. `Joystick` / 摇杆 (非原生模式下，使用键盘 `ASDW` 控制移动)

. `jsdoc` 这次编码时，尽量按照 `jsdoc` 规范编码，提供在线文档

![ssrlos15](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos15.png)

### Compile / 编译

开发环境:

`macOS Catalina 10.15.6 `

`cocos2dx v3.17.2 `

`Creator 2.4.0`

#### iOS for cocos2dx

`Xcode Version 11.6 (11E708)`

`Xcode` 直接编译运行即可。

#### android for cocos2dx

安卓使用 `ndk-build` 会报错，应该是引擎的问题:

> [升级到cocos2d-x-3.17.2，安卓CCActionProgressTimer.cpp编译不过](https://forum.cocos.org/t/cocos2d-x-3-17-2-ccactionprogresstimer-cpp/80062)

我这里就注释掉报错的部分继续编译了，因为是 `ProgressTimer` 的部分，我并不需要。

```shell
# ndk-build 编译 android-ndk-r13b
cocos compile -p android -m release --build-type ndk-build

# cmake 编译 3.10.2.4988404
cocos compile -p android -m release --build-type cmake
```

#### jsdoc

执行 `tools/jsdoc`  下的 `generate_doc.py` 即可，生成路径

 `SSRLoS-Cocos2dx/tools/jsdoc/docs/index.html`

#### min 压缩

进入 `tools/publish` 下执行 `ant` 即可，生成路径

`SSRLoS-Cocos2dx/tools/publish/SSRLoS.min.js`

## Performance Test / 性能测试

正如前面说到过的，`LoS` 本身就是非常耗性能的功能，加上各种渲染操作，更是对优化有非常高的要求。

这里的测试对象，涵盖了原生，非原生，尽量包含了各种操作系统，手机类型，品牌，系统版本等等。

测试用例选用了3组不同复杂度的情况。

### Test Model / 测试模型

#### Model.1

`2018 产`

`MacBook Pro (13-inch)`

`macOS Catalina 10.15.6 `

`Google Chrome 版本 85.0.4183.83（正式版本）（64 位）`

#### Model.2

`2018 产`

`MacBook Pro (13-inch)`

`macOS Catalina 10.15.6 `

`Firefox 80.0.1 (64 位)`

#### Model.3

`2018 产`

`MacBook Pro (13-inch)`

`macOS Catalina 10.15.6 `

`Safari 版本13.1.2 (15609.3.5.1.3)`

#### Model.4

`2017 产`

`iPhone X 13.3.1 MQA92CH/A `

#### Model.5

`2014 产`

`iPhone 6 11.3.1 MG4H27P/A`

#### Model.6

`2014 产`

`iPhone 6 Plus 12.1 MGAK2CH/A`

#### Model.7

`2013 产`

`SamSung Galaxy S4 4G GT-I9507V Android 5.0.1`

#### Model.8

`2017 产`

`Huawei Honor 7X BND-AL10 Android 9.0 EMUI 9.1.0`

#### Model.9

`2018 产`

`OPPO A5 PBAM00 Android 8.1 ColorOS V5.2.1`

### Test Cases / 测试用例

所有原生环境下开启 `JSBinding` 功能，否则下面这些用例几乎都是跑不动的。

#### Case.01 ~ 04 (复杂度高)

`定点数 600` + `10 照明光源 / 动态` + `1 主光源 / Render Sight Area`

`100 Obstalces` + `10 Lights (Move)` 

Case.01 - `Unlimited Range`

Case.02 - `Unlimited Range` + `Dirty Direction`

Case.03 - `Full Angle`

Case.04 - `Full Angle` + `Dirty Direction`

#### Case.05 ~ 08 (复杂度中)

`定点数 300` + `4 照明光源 / 动态` + `1 主光源 / Render Sight Area`

`50 Obstalces` + `4 Lights (Move)` 

Case.05 - `Unlimited Range`

Case.06 - `Unlimited Range` + `Dirty Direction`

Case.07 - `Full Angle`

Case.08 - `Full Angle` + `Dirty Direction`

#### Case.09 ~ 12 (复杂度低)

`定点数 180` + `2 照明光源 / 动态` + `1 主光源 / Render Sight Area`

`30 Obstacles` + `2 Lights (Move)` 

Case.09 - `Unlimited Range`

Case.10 - `Unlimited Range` + `Dirty Direction`

Case.11 - `Full Angle`

Case.12 - `Full Angle` + `Dirty Direction`

| Cocos2d-x | C01  | C02  | C03  | C04  | C05  | C06  | C07  | C08  | C09  | C10  | C11  | C12  |
| :-------: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: |
|  Chrome   |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |
|  Firefox  |  40  |  30  |  50  |  38  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |
|  Safari   |  52  |  42  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |
| iPhone 6  |  22  |  0   |  20  |  0   |  42  |  16  |  54  |  18  |  58  |  48  |  60  |  58  |
| iPhone 6P |  25  |  0   |  32  |  0   |  58  |  25  |  60  |  27  |  60  |  50  |  60  |  54  |
| iPhone X  |  60  |  20  |  60  |  24  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |  60  |
| Galaxy S4 |  10  |  0   |  20  |  0   |  35  |  10  |  56  |  12  |  57  |  20  |  60  |  24  |
| Honor 7X  |  18  |  0   |  24  |  0   |  45  |  25  |  60  |  28  |  60  |  48  |  60  |  58  |
|  OPPO A5  |  13  |  0   |  18  |  0   |  37  |  18  |  58  |  23  |  58  |  38  |  60  |  42  |
|           |      |      |      |      |      |      |      |      |      |      |      |      |

`Creator v1/v2` 的数据暂时没有整理贴在这里，总体来说 `v1/v2` 在非原生部分，在最复杂的测试用例下，接近于 `cocos2d-x` ，相差 `5 ~ 10 fps` 左右。

在原生部分，`v2 > v1` 然后和 `cocos2d-x` 相比还是有一定差距，当然这也可能是我代码实现上存在的问题造成的（`v2` 只有计算部分做了绑定，渲染还是在 `js` 完成的），因为我对 `creator` 的熟悉程度不如 `cocos2d-x`。

### Mention / 注意

前面曾经提到过 `LoS` 最初的版本只是为了在 `Tiled Map` 中进行使用，最初的 `Demo` 中还包含有一个 `Tiled Map` 编辑器，以及 `Connected Component Labeling`  `Tile Contours Extract` 算法相关的代码。

但是为了保证 `Demo` 功能的纯净以方便阅读，这些和 `LoS` 并非必要的相关功能已经被去掉。


## Algorithm Overview / 算法概述

### Basic Target / 基础目标

`LoS` 的基础算法，目的十分明确:

```javascript
给定一个光源，给定光源的相关参数(范围，角度之类)，再给定一些障碍物的描述信息(多边形顶点)，计算出从光源能够观察到的，实际形成的多边形区域(多边形顶点)。
```

### Advanced Target / 进阶目标

通过 `LoS` 算法，得到给定对象的可见区域，是算法的本意，也是基础，但是从实际运用的角度出发，还会有一些其他的需求，而这些需求又会促使算法生成新的目标，从而需要我们在算法中收集，输出更多的信息。

#### Target Visibility

目标的可见性，一个极其常见的需求，既然已经获取了给定对象的可见区域，那么对于这个对象而言，世界中的哪些目标是可见的，哪些目标是不可见的，自然是可以通过 `LoS` 算法收集到的信息来判断的。

#### Visible Edge Tracking

可视边追踪，`LoS` 算法计算出了可视多边形区域，那么从我们的对象出发，实际可视的障碍物的边又是哪些呢。换句话说，从一个光源发出的光线，实际照亮的，是哪些障碍物的墙壁呢。

#### Full Data Output

通常的算法仅仅输出可视范围的多边形信息，但是为了能够更好的使用该模块进行二次开发，尽可能保留算法过程中的有意义数据，并进行输出。

### Workflow / 流程

在了解完目标后，并且进行了一些相关知识点的搜索和学习之后，了解到针对不同的情况，也有各种不同的算法来进行处理。这里所说的不同情况，指的是:

. 不同的视线范围类型

. 不同的障碍物类型

. 不同的地图类型

. 精度需求

. 等等

作为入门，也为了可以涵盖更多的使用场景，我选择了非常常见的 `射线检测`作为核心方式来完成整个算法。

接下来，对整个算法的视线做一个流程化的说明，这主要分为了几个大的阶段:

. Stage.0 数据准备阶段，对算法所需的输入数据进行完整的收集和合理的组织

. Stage.1 剔除算法阶段，初期对障碍物筛选的重要步骤，对算法优化有着极大的影响

. Stage.2 射线生成阶段，以上一阶段剔除后的障碍物为标准，开始生产射线

. Stage.3 射线投射阶段，实际发射射线，与障碍物进行相交判断，寻找交点

. Stage.4 交点合并阶段，最大限度的合并交点，为渲染部分的优化做好准备

. Stage.5 渲染阶段，经过前面一系列的计算，做种的目的当然是把结果数据以可视化的方式呈现给玩家

![ssrlos14](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos14.png)

## Stage.0 Preparation / 准备

对于 `LoS` 模块来说，最重要的是两个组成部分 `宿主/光源` 和 `障碍物`。

### Source / 宿主

作为光源，应该需要能够收集以下的信息。

. 视线类型，视距，角度

对于 `cocos2dx` 来说，一切宿主都可以说是 `cc.Node` 的子类，比如 `cc.HeroObject`, `cc.CandleObject`。这里在设计时，参考了 `Creator` 的组件模式，通过给已有类增加 `LoS` 组件，来进行扩展，为其增加 `视线` 的功能。

既然这里提到了宿主是 `cc.Node` 的子类，那么宿主本体的一些属性自然也就需要关注了:

. 位置 / 旋转 / 缩放 / 扭曲 / 可见性

### Obstacle/ 障碍物

宿主有了，`视线` 的功能也开启了，那么接下来就是轮到 `障碍物` 了。和 `宿主` 相同，`cocos2dx` 中，`障碍物` 基本上也都应该是 `cc.Node` 对象的子类，比如 `cc.Wall` ， `cc.Tree`。

对于 `LoS` 这里所最关心的，肯定是 `障碍物` 所占据的 `空间范围` 了，而这在程序的体现上，可以抽象为 `多边形数组`。

任何形状的 `障碍物` 都必然可以用 `多边形顶点数组` 来描述，如果是非闭合图形，那就是一条 `折线`。[OPT]

`障碍物` 也可以大致的分为 `静态` 和 `动态`两种，或者更直接的说，就是 `顶点数组` 是否会发生改变。这对于优化算法也会起到非常大的作用。[OPT]

### 模块设计

#### 计算，渲染分离

有时我们可能只是需要计算出 `LoS` 的可视范围或是其他一些输出数据，但是并不需要进行实际的渲染，因此将这些功能点进行分离是非常必要的。[OPT]

#### 引擎独立性

对于渲染部分，很难做到引擎独立，换个引擎，渲染的实现方式代码必然需要变化，但是对于计算部分来说，是可以做到使用纯 `javascript` 来实现的。不过我这里目前的实现并没有做到完全的独立，还是在算法部分依赖了 `cc.Node` 类。

### 易用性

隐藏所有复杂的细节，暴露足够丰富的接口。

#### 对已有对象的扩展:

```javascript
// 宿主
cc.HeroObject = cc.Node.extend({
  	ctor:function() {},
  	render:function() {},
  	attack:function() {},
		walk:function() {},
		// 扩展视线功能
		addLoSComponent:function() {
      	this._losComponentCore = new ssr.LoS.Component.Core(this);
    },
		getLoSComponent:function() {
      	return this._losComponentCore;
    },
  	update:function)() {
  			// ......
  			this._losComponentCore.update();
		}
});
// 获取视线相关数据
heroObject.getLoSComponent().getSightArea();

// 障碍物
cc.WallObject = cc.Node.extend({
  	ctor:function() {},
  	render:function() {},
		// 自定义的顶点数组返回函数，可以不提供，则默认为 boundingbox
		vertexArrayProvider:function() {
				return [......];
    }
});
heroObject.getLoSComponent().addObstalce(wallObject, wallObject.vertexArrayProvider);
```

#### 渲染部分 `API`

```javascript
// 渲染射线
var losComponentRenderRay = new ssr.LoS.Component.RenderRay(heroObject.getLoSComponent());
this.addChild(losComponentRenderRay);

// Mask
var losMaskNode = new ssr.LoS.Component.Mask(MASK_COLOR);
this.addChild(losMaskNode);
// 加入 LoS 对象
losMaskNode.addTarget(heroObject, heroObject.getLoSComponent);

// 需要时更新渲染组件
losComponentRenderRay.plot();

// 更新 Mask，会自动按需更新
losMaskNode.updateTarget(heroObject);
```

## Stage.1 Culling / 剔除

### Performance Issues / 性能问题

网上大部分流传的算法，就是前面提到的射线法:

```javascript
向所有障碍物端点发射射线，每个端点发射3条射线，端点1条，顺/逆时针旋转一个极小角各发射1条
```

在不进行任何优化的前提下，对于障碍物不多的情况，还是可以比较好的应对的，但是当世界中的障碍物增多，算法效率就会变得无比低下。

考虑一个射击游戏，假设只有一个主角，障碍物数量是 50 个独立的，不相邻的正方形。那么上述算法的计算量大致如下:

``` javascript
. 顶点数 => 50 * 4 = 200
. 边数 => 50 * 4 = 200
. 射线数 => 200 * 3 = 600
. 每条射线求出最近交点需要的相交计算次数 => 200
. 计算出所有交点需要的相交计算次数 => 600 * 200 = 120000
```

可以看到，计算一次可视区域范围需要 12万次的计算，这还只是一次计算，对于一款射击游戏而言，主角每帧移动是必然的，因此这 12万次的计算量会每帧都发生，这样的游戏的流畅度结当然是可想而知的。

更不用提实际的射击游戏中，障碍物还有可能会动态的变化，除了主角以外，一些敌人也同样可能需要使用 `LoS` 算法来计算其可视范围，再加上游戏的其他寻路，碰撞，子弹，AI，这样的算法是没有任何的实用性可言的。

从上一节的例子可以看到，要提高算法的效率，最简单的方法，自然是减少那 12万次计算量。

简单推导一下:

``` javascript
假设障碍物的数量是 N
每个障碍物的顶点数 M
顶点总数 M * N
边总数 M * N
射线总数 M * N * 3
总计算次数 (M * N * 3) * (M * N) = M² * N² * 3
```

这样一来就显而易见了，每个变量的减少，都可以大幅度的减少计算量。

尽可能的减少参与计算的顶点的数量，或是减少边的数量，或是减少射线的数量，都是提高算法效率的方式。

这种技巧，称为 `Culling` 剔除，在算法初期剔除掉不需要的信息，是极其重要的。

某些情况下，比如视线的范围是无限，无法简单的进行剔除，那么所有的障碍物的边和顶点都是需要保留的。

对于视线范围是有限的，如圆形，或是有角度限制的扇形，那么就可以通过剔除，来去掉很多不需要的数据了。由于剔除的过程，往往不需要做精确的交点计算，只需要通过点线之类的关系，就可以判断是否需要保留数据，因此效率还是非常高的。

### 核心思想

总结下 `Culling` 的做法就是:

. 从所有的输入边中，找出有效的角点
. 从所有的输入边中，找出潜在障碍边
. 尽可能在算法的初期，通过剔除的手段，减少交点和潜在障碍边的数量，为后续的碰撞点计算做优化

#### End Point / 端点

端点的剔除相对简单，基本上只需要和视线区域做包含关系测试。
如果包含则必然保留，成为 `End Angle Point`。
如果不包含，则必然不需要保留。

#### Edge / 边
边的剔除略微复杂，首先需要获取边的两个端点和视线区域做包含关系测试的结果。
如果边的端点有一个在视线区域内，则这条边必然保留，成为 `PotentialBlockingEdge`。
如果两个端点都不在视线区域内，那么就需要通过边和视线区域做各种相交测试，来判断这条边是否可以保留了。

#### Intersect Point / 交点
除了边的端点可能形成 `End Angle Point` 被保留，部分障碍边可能会和视线区域形成新的交点，这些交点有的需要保留，形成 `Boundary Angle Point`。

#### Critical Condition / 临界情况
某些端点可能处于视线区域的某些临界位置。如端点在矩形的边上，在圆或是扇形的圆弧上，在扇形的两边上，等等。这些情况，都是需要有不同的处理的。

### Cautious / 注意点
. 对于每一种不同的视线区域类型，细分所有可能出现的情况类型，一一进行不同的处理。
. 剔除的结果需要去重，对于可能出现的重复 `Angle Point` 无需重复保留。
. 一些复杂的情况，可能需要好几种测试算法，尽量按照算法的复杂度， 从低到高来使用，提高效率。
. 继续上一点，把剔除算法分为两类，粗测(`Broad Phase`)，细测(`Narrow Phase`)。粗测只进行最快速的位置关系判断，细测则进行相对复杂的，准确交点的计算。

### TestCase / 测试用例

剔除算法是整个算法最为关键的部分，必须做到对所有的视线类型及其各种相交情况都有准确无误的处理。这一部分的情况多而复杂，有兴趣的可以参见单独的文档: [SSRLoS_Culling.md](http://supersuraccoon.gitee.io/ssrlos-doc/SSRLoS_Culling.md) 。

## Stage.2 Ray Generate / 生成射线

在剔除步骤之后，就是要开始生成射线的部分了，生成射线时，需要尽量减少数据量。

### Basic Algorithm / 基础算法

`LoS` 的算法有很多，根据应用场景的不同，障碍物的数量，类型，可变性等等属性，可以有各种计算方式，这里来看下最基础的算法。

#### Full Angle Raycasting / 全角度射线法

最简单粗暴的方式。算法的流程描述如下:

```javascript
1. 以光源为原点，逆时针或顺时针发射出 N 条射线
2. 每条射线和所有的障碍物多边形的边求交点
3. 每条射线找出距离原点最近的那个交点
4. 最后将所有的交点一次相连，得到的多边形区域就是所求的可见区域
```

算法十分简单，当然缺点也是显然的，算法的准确度取决于 `N` 的取值，而算法的复杂度也取决于 `N` 的取值 (当然同时也取决于障碍物边的数量)。

`N` 取值太小，会导致算法的精度是不可靠，`N` 取值太大会导致算法效率极低。除非只是一些障碍物数量少，静态的场景，通常不会使用。

#### EndPoing Raycasting / 端点射线法

假设障碍物只有一个小小的正方形的世界，全角度发射 `N` 条射线显示是浪费，一个很容易想到的优化， 就是只向障碍物边的端点发射射线，当然只是这样的话是无法获得准确的可视区域的，还需要以射向端点的射线为基准，顺时针逆时针微调极小的角度，再发射两条辅助射线，才能确保结果的准确。

算法的流程描述如下:

``` javascript
1. 以光源为原点，向障碍物多边形的所有端点发射射线
2. 以每条射为基准，线进行角度的微调，再发射两条辅助射线
3. 顺时针或逆时针排序所有的射线
4. 每条射线和所有的障碍物多边形的边求交点
5. 每条射线找出距离原点最近的那个交点
6. 最后将所有的交点一次相连，得到的多边形区域就是所求的可见区域
```

这种算法得出的结果，是精确的，辅助射线可以保证可视区域的结果在每个障碍物的角落都能够准确无比，不会存在缺角的情况。

### Auxiliary Ray / 辅助射线

上一阶段的 `Culling` 已经尽量对边和顶点进行了剔除，在这里，对于射线的 `*3` 也是可以进行优化的，因为对于大部分的端点而言，其实只需要1条辅助射线就足够了，因为另一条必然会和边相交。

这里我参考了这篇文章中关于射线部分的优化方式，来减少生成射线的数量:

> [Field of View and Line of Sight in 2D](https://legends2k.github.io/2d-fov/design.html)

在发射射线之前，我们首先要生成一些辅助射线，这些射线以射向端点的射线为基准，通过逆时针/顺时针旋转一个极小的角度生成。

在不进行任何优化的前提下，对于一个端点需要3条射线 (1条主射线 + 2条辅助射线)，但是通过优化后，大部分的端点都只需要2条射线 (1条主射线 + 1条辅助射线)，部分端点只需要1条射线 (1条主射线)，只有极少的情况下，端点需要3条射线。

对于生成辅助射线，需求如下:

``` javascript
. 给定一条射线(原点，射线上一点)
. 顺时针/逆时针旋转指定的角度 α，生成新的射线
. 返回新的射线上的一点的坐标
```

### Ray Sort / 射线排序

完成了所有射线的生成以后，就需要开始来进行一次射线的排序了。需求如下:

``` javascript
. 给定原点
. 给定一些带排序的点
. 从原点出发，向每个待排序的点发出射线
. 起点随意，顺时针/逆时针，按照射线的角度，排序
```

### Ray Culling / 射线剔除

到这里，所有的射线已经排序完成，但是由于这些射线中，可能存在`同向共线` 的情况，因此还需要做一次简单的剔除。做法如下:

``` javascript
. 循环检测所有已排序的射线
. 对于前后两点同向共线的射线，只保留距离原点最近的一条射线
```

#### 总结

到这里，射线的预备准备工作就结束了，后面的工作就是性能消耗的最大步骤，因此在这一步的处理中，必须再一次的去除可能的冗余信息，为下一步的相交计算做好准备。

## Stage.3 Ray Casting / 射线投射

射线准备就绪，接下来的步骤，自然就是发射射线，和障碍边做相交计算，求出实际的碰撞点了。

通常的做法如下:

``` javascript
. 依次循环每一条射线
. 每一条射线和所有的障碍边进行相交计算
. 每条射线只保留距离光源最近的碰撞点
```

这样的做法毫无疑问是正确的，但是还是有极大的优化空间的。一些很快就能想到的优化:

``` javascript
. 对于射向端点的射线，交点可能就是自己，不需要和包含这个端点的任何线段做相交测试
```

当然这还远远不够，下面是最大的优化:

``` javascript
. 首先把障碍边分为两种
. 如果碰撞点就是端点自身，那么认为没有有效障碍边
. 如果碰撞点不是端点自身，那么把这条边记录为有效障碍边
. 对于任意一条射线，如果它之前的射线没有记录到有效障碍边，那么算法照旧
. 对于任意一条射线，如果它之前的射线有记录到有效障碍边，那么只需要和这条有效障碍边做一次相见计算，如果有交点，那么交点必然就是最有的碰撞点；如果没有交点，那么射线的端点自身，就必然是最优的障碍点
```

通过上面的优化，原先的相交计算的次数可以被大幅度的降低。除了上面所提到的，在实际算法的调优中，还做了不少细节的处理，可以参看 `ssr.LoS.Strategy.Process.Base::castRays` 的核心实现。

## Stage.4 Intersection Merge / 交点合并

在经过射线投射和交点的计算后，组成可视范围的多边形顶点数组已经可以成功获取到了，但是这时候如果直接进行如一步的渲染阶段的话，那么效率将会非常的低下，因为有很多共线的交点，是可以进行合并，也是必须合并的，只有这样才能将需要渲染的三角形的数量减少到一个可接受的范围，以提高渲染的效率。

交点合并的目标是非常简单明确的，对于处于同一条边上的所有**连续**交点，只需要保留位于**两个端点**的两个点击可。

这里的算法就非常简单了，因为前面的阶段在求出交点的时候同时也记录了**所属边的 ID**，只是对于**限制视线范围**的类型 (圆形，扇形)，要注意**障碍物**上的交点和**弧线**上的交点的处理。

## Stage.5 Render / 渲染

### `FoV` / 视野范围 (`Mask`)

这里的 `FoV` 是 `Field Of View / Vision` 的缩写，也就是视野范围的意义。

`LoS` 的最大任务，其实可以说是纯粹的 `计算`。通过对输入的物体的坐标，视线属性以及世界中的障碍物的信息进行层层筛选和处理，最终输出可视区域的信息。

可视化信息的数据到手后，那就是要考虑如何将其渲染到屏幕上的时候了。

就像前面所谈到的，`LoS` 的目的就是为了计算出玩家在有障碍物的情况下，实际可见的地图区域信息。

最容易想到的一种实现方法，就是在整个地图上覆盖黑色的遮罩，然后只将可视区域的内容进行渲染。

在 `cocos2dx` 中，想要达到这个效果，也有不同的实现方式，当然难易度和执行效率则是不尽相同。

#### cc.DrawNode + cc.RenderTexture + Blending Mode

关于这方面的教程网上有很多，实现起来也不困难。虽然可以达到效果，但是实际运行起来，性能相当的不理想，因为 `cc.RenderTexture` 在频繁的进行 `clear/draw` 的操作。

#### cc.DrawNode + cc.LayerColor + cc.ClippinNode

这种组合的实现方式也是非常简单直接，使用`cc.ClippingNode `的 `invert mask`功能即可。这种方式的运行性能是相当理想的。

####Multi-Sources / 多光源

`cc.ClippinNode` 的实现方式已经达到了想要的效果，但是为了支持多光源，还是需要进行一些合理的设计的。

. 游戏场景中，应该尽可能保证只有一个 `cc.ClippinNode` 实例
. 通过一个 `cc.ClippinNode` 实例，来渲染所有可能的光源
. `cc.ClippinNode` 的大小，应该和屏幕大小相同
 . 即使在大地图的情况下，也应该尽量做到不调用 `setPosition` 来提高效率

##### 设计一

```javascript
假设有光源 A,B,C,D,E,F,G (LightSources => Ls)
以下为渲染逻辑的操作:
. 每次先清除 cc.ClippinNode，再重新渲染

cc.ClippinNode 的结构设计

MaskNode
		_maskClipper (Child / cc.ClippingNode)
				_maskLayer (Child / cc.LayerColor)
				_maskStencilNode (Child / Stencil (invert) / cc.Node)
						_maskDrawNode (Child / cc.DrawNode)

执行逻辑，所有光源通过一个 maskDrawNode 来渲染，

update:function(dt) {
		for (l in ls) {
				anyDirty = l.updateSight();
  	}
  	if (anyDirty) {
    		Mask.clear();
		    for (l in ls) {
      			Mask.render(l.sight);    
    		}
  	}
}
```

这种做法的缺陷在于，即使只有一个光源 `Dirty`，必须重新 `render` 所有光源。

##### 设计二

针对上面这种设计的缺陷，对 `StencilNode` 做出修改，为每个光源独立创建 `maskDrawNode`

```javascript
MaskNode
		_maskClipper (Child / cc.ClippingNode)
				_maskLayer (Child / cc.LayerColor)
				_maskStencilNode (Child / Stencil (invert) / cc.Node)
						_maskRenderA (Child / cc.DrawNode)
						_maskRenderB (Child / cc.DrawNode)
						_maskRenderC (Child / cc.DrawNode)
				  	......
            
update:function(dt) {
		for (l in ls) {
    		dirty = l.updateSight();
	    	if (dirty) {
      			Mask.render(l.sight);
    		}    
  	}
}

可以看到，每个光源对应一个独立的 maskRender，这是一个 cc.DrawNode 对象，且是 Mask 的 Stencil 容器的一员，下面看个实际的例子。

1. Mask 的初始状态(无光源):
		_maskClipper (Child / cc.ClippingNode)
				_maskLayer (Child / cc.LayerColor)
				_maskStencilNode (Child / Stencil (invert) / cc.Node)
此时， Mask 呈现完整的 maskLayer

2. 增加一个光源A
		_maskClipper (Child / cc.ClippingNode)
    		_maskLayer (Child / cc.LayerColor)
        _maskStencilNode (Child / Stencil (invert) / cc.Node)
        		_maskRenderA (Child / cc.DrawNode)
此时，新增一个 maskRenderA，这是一个 cc.DrawNode 对象，且是 Mask 的 Stencil 容器的一员，今后光源 A 的更新，都只会更新对应的 maskRenderA

3. 增加n个光源
		_maskClipper (Child / cc.ClippingNode)
    		_maskLayer (Child / cc.LayerColor)
        _maskStencilNode (Child / Stencil (invert) / cc.Node)
        		_maskRenderA (Child / cc.DrawNode)
            _maskRenderB (Child / cc.DrawNode)
            _maskRenderC (Child / cc.DrawNode)
						.....
此时，各个光源，根据各自的更新状态，更新对应的 _maskRender，互不影响

4. 删除光源 A, B
		_maskClipper (Child / cc.ClippingNode)
    		_maskLayer (Child / cc.LayerColor)
        _maskStencilNode (Child / Stencil (invert) / cc.Node)
        		_maskRenderC (Child / cc.DrawNode)
            ......            
此时，只要删除对应的 _maskRender，即可
```

![ssrlos7](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos7.png)

![ssrlos8](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos8.png)

![ssrlos9](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos9.png)

### Illumination / 光照

`LoS` 可以挂在到任意的 `cc.Node` 对象上，在渲染视线范围时，如果再配合上光照的效果，就可以模拟出光源的效果。

`cocos2dx` 中渲染绘制图形主要使用的是 `cc.DrawNode` 对象。当然单纯的使用 `cc.DrawNode` 进行渲染是无法模拟出光照的效果的，至少需要加上渐变的效果。

这里采取的简单的做法，就是继承 `cc.DrawNode` 并重新实现其 `Fragment Shader`，将要进行渲染的内容做一个径向的渐变效果。

需要注意的是，在多光源重叠的情况下，需要设置合适的 `Blending Mode` 才能使得效果变得真实。

```javascript
// cocos2dx 内置变量，通过尝试搭配，可以找到最好的效果
cc.ONE = 1;
cc.ZERO = 0;
cc.DST_COLOR = 0x306;
cc.SRC_COLOR = 0x300;
cc.ONE_MINUS_DST_COLOR = 0x0307;
cc.ONE_MINUS_SRC_COLOR = 0x301;
cc.SRC_ALPHA = 0x0302;
cc.ONE_MINUS_SRC_ALPHA = 0x0303;
cc.DST_ALPHA = 0x304;
cc.ONE_MINUS_DST_ALPHA = 0x305;
cc.SRC_ALPHA_SATURATE = 0x308;
cc.ONE_MINUS_CONSTANT_ALPHA	= 0x8004;
cc.ONE_MINUS_CONSTANT_COLOR	= 0x8002;
```



> [2D omnidirectional shadow mapping with Three.Js](https://csantosbh.wordpress.com/2014/03/29/2d-omnidirectional-shadow-mapping-with-three-js/)

![ssrlos10](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos10.png)

### Shadow

阴影效果，和 `Mask` 有着不同的使用场景和呈现效果。 

`Mask` 主要用于做视野范围的遮罩，关注的是**可见的部分，不可见的部分**。通常需要关注场景中的所有光源和障碍物。

`Shadow` 主要用于显示动态阴影，一般只需要关注制造阴影的光源和自身(障碍物)。

目前主要做了两种实现方式。

#### Mask + Shader

......

#### Mask + Texture

......

## Develop Log / 开发日志

正如前面提到的，这个功能模块在开发过程中进过了很多次的重构，优化，这里主要是记录这些版本的衍变过程。

### 1st Version

最初的版本，没有任何的设计，只是单纯的完成任务，不在意任何的效率，易用性之类的问题，只是从`可行性层面`证明这个模块功能是可以被实现出来的。

这一版的内容完全参考了网上的这篇教程: 

> [SIGHT & LIGHT / how to create 2D visibility/shadow effects for your game](http://ncase.me/sight-and-light/) 

![ssrlos18](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos18.png)

该版本只支持 `无限制视线距离 / 角度`视线类型，在浏览器(`Mac Chrome / Safari`)和原生环境下(`iOS /android `)进行了测试。

![ssrlos1](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos1.png)

![ssrlos2](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos2.png)

![ssrlos3](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos3.png)

### 2nd Version

开始支持 `限制视线距离 / 不限制角度` 视线类型。同时也在在一些`Tiled Map` `Demo` 中尝试了实际的使用。

在第一版完成后，我在网络上继续寻找相关的文章，最后找到了这篇:

>   [2d Visibility from Red Blob Games](http://www.redblobgames.com/articles/visibility/)

虽然这篇文章只谈到了  `无限制视线距离 / 角度`视线类型，但是其中谈及到的一些数学概念和理论基础给了我很大的启发。再次基础上，我自己尝试实现了 `限制视线距离 / 不限制角度` 视线类型。

![ssrlos4](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos4.png)

![ssrlos5](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos5.png)

### 3rd Version

开始支持所有视线类型。

继续通过在网上搜索相关文章，找到了下面这篇非常赞的文章，其中描述了扇形视线范围的视线原理，同时还提出了很多优化相关的概念:

>    [Field of View and Line of Sight in 2D](https://legends2k.github.io/2d-fov/design.html)

这一版本重构代码，整体设计上采用了 `策略模式`，使得针对每一种的视线类型算法独立，更便于优化和管理，同时该版本还进行了第一次大型的优化，主要的内容包括:

.  `Culling` 算法部分的重度优化，配合 `策略模式`，针对不同的视线模式进行不同方式的剔除

. `Ray Generate / Casting` 算法部分的重度优化

​		.. `起点 / 终点射线`，无需 `±RADIANS_DELTA` (减少2条射线计算量)
​		.. `光源<>顶点射线` 与 `多边形边` 相交计算，如交点在线段上，无需 `±RADIANS_DELTA` (减少2条射计算量)
​       .. `光源<>顶点射线`与`多边形边`相交计算，直接过滤掉顶点所属的边
​       .. `交点数组`排序优化
​       .. 过滤掉角度相同的`光源<>顶点射线` (减少3条射线计算量)
​       .. 过滤掉相同的顶点 (减少3条射线计算量)
​            EdgeA 的起点 可能等于 EdgeB 的终点，如一个矩形的 EdgeA 和 EdgeB
​            但是不能只检测起点，因为 EdgeB 可能被剔除，导致 EdgeA 的终点检测的缺失

. 对于各种不同形状之间的`相交算法` 的重度优化

​		.. 严守`快速测试`的原则，在算法初期以最简单的判断排除掉尽可能多的情况

​		.. 分清需求，对于无需获取准确交点的情况，绝不进行无意义的多余的运算

![ssrlos6](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ssrlos6.png)

### 4th Version

再次的大幅优化，主要的内容包括:

. 尽可能少的使用三角函数

. 尽可能使用向量计算

. 尽可能少的计算实际的交点而是使用向量快速的进行位置关系测试

. 尽量使用 fuzzyEqual，由于精度的问题，这样可以避免现临界情况的测试结果不准确

. 尽量少使用三角函数

. 对于所有临时数据对象 (`Edge`, `HitPoint`, ......) 采用对象池

. 尽可能减少重复的计算，缓存值

这一版的优化后，即使是相对复杂的地图，在网页上也能有非常好的 `FPS` 表现，但是在原生部分，帧率的表现还是并不很理想。

### 5th version

这一版本主要针对原生平台进行优化，主要内容包括:

. 减少使用 `cocos` 内置的一些需要经过绑定的函数和数据类型，改用原生的实现
		.. 减少 JS 与 C++ 数据类型的互相转化的性能消耗
		.. 减少 JS <-> C++ 传输数据量
		.. 降低 JS <-> C++ 传输数据频度

. 实现原生的 `LoS` 算法 `C++`

. 实现 `LoS - JSBinding` 

在完成了原生层的完整绑定后，原生环境下的运行效率已经完全可以达到媲美网页端的效率了。

### 6th version

随着 `CocosCreator` 开始慢慢变成主流，我也开始尝试将 `SSRLoS`进行移植。

因为最初在进行模块设计的时候就参考了 `Creator` 的组件设计理念，因此在对外接口 `API` 这块设计上是无需进行太多新的工作的。

至于算法方面，这一块完全是和引擎无关的纯计算，移植自然也是非常的方便，几乎不需要任何的修改。

最麻烦，或者说工作量最大的部分，自然就是渲染方面了，毕竟这一块 `Creator` 和 `Cocos2dx` 还是有相当差距的，但是毕竟是有传承的关系在的，所以只要找到合适的替代模块，移植也还是完全可以做到的。

这一版主要将模块移植到 `Creator 2.x` 并且配合新的 `JSBinding` 接口规范，重写了绑定层。

___

以上都是至今为止开发的一些版本的内容，下面开始记录的是近期 (`2020.10` 后) 的开发内容: 

[SSRLoS_Develop_Log.md](http://supersuraccoon.gitee.io/ssrlos-doc/SSRLoS_Develop_Log.md)

## Issue List / 问题清单

由于模块功能复杂，细节点多，同时对应多平台多引擎分支，因此肯定存在很多的问题，这里进行简单的记录，并尽量一一修复: 

[SSRLoS_Issue_list.md](http://supersuraccoon.gitee.io/ssrlos-doc/SSRLoS_Issue_list.md)

## TOTRY / 待尝试

`LoS` 这样一个库断断续续也写了3年了，虽然实际花在上面的时间没这么夸张，但是想要优化或者说尝试的地方还是有很多的。下面列出的就是这样一些技术点。

### Ray-casting + Triangulation + Bresenham

> [Umbrella Development Log Beamcasting](http://sadumbrella.blogspot.se/2010/11/beamcasting.html)
>
> [Walls and Shadows in 2D](https://www.gamedev.net/articles/programming/graphics/walls-and-shadows-in-2d-r2711)
>
> [Raytracing on a grid](http://playtechs.blogspot.com/2007/03/raytracing-on-grid.html)

除了现在我使用的方法之外，还有一些别的算法，当然这对于地图和障碍物可能会有一些特殊要求，比如需要是 `TiledMap` 才能够使用，或者说才适合使用。但是换句话说，只要满足了条件，这样的算法的效率肯定是高于现在算法的，毕竟是 `量身定做` 的算法。

### Obstacle Merge / 障碍物合并

正如前面提到的，障碍物也有`动态`的，这也意味着有些障碍物存在着`Overlapping / 重叠`的情况存在，这时候通过合并障碍物，组合成新的障碍物也可以减少计算量，当然这里需要权衡是**合并的开销大，还是计算的开销大**。

复数 `Tile` 的动态合并  `Connected Component Labeling` 以及轮廓提取 (**已经实现**)

复数多边形的动态合并，使用 `Polygon Clipper Union`

### Less Triangulate Render / 减少三角化渲染

目前的`视线范围渲染` 只是多到了将**可合并成三角的小三角形进行合并**这一步，想要进一步提高渲染的效率，那自就应该将 `可合并成凸多边形的三角形合并`，这样就可以进一步减少每帧需要渲染的数量了。

#### Better Collineation Culling / 优化共线剔除

. `Culling` 的处理中，过滤掉了可能出现的重复的端点，但是对于存在于同一射线上的端点，却是在后期生成射线的步骤中，才过滤掉。这期间经历了 `生成辅助射线` 的步骤。

考虑是否可以在 `Culling` 的阶段就过滤掉，如:

```javascript
. 对需要保存的端点，计算斜率，象限
. 用 斜率_象限 的形式作为 key
. 判断待添加的端点，是否有已经存在的 斜率_象限
. 没有，则直接保存
. 有，则进一步计算距离平方，只保存一个距离光源更近的端点
```

目前暂时没有尝试，因为精度的问题，使用斜率作为 `Key`，很少的情况才会出现命中的情况，换句话说，这种机制可以剔除的共线情况会很少。

需要寻找更好的剔除共射线的方式是最好的。暂时没有尝试。

### Better Sort / 优化排序

目前的 射线 `sort` 的处理中，目前是用了三角函数计算弧度，再使用 `javascript` 内置的 `sort` 函数进行排序的方法。考虑到三角函数的计算会影响效率，因此尝试使用向量的方式来完成排序，但是实际尝试后发现效率并没有多大提升，因此目前还是保持三角函数的做法。

此外内置 `sort` 的效率也许不够好，考虑改成一些主流的排序算法， 暂时没有尝试。

### Better Hashcode / 优化哈希码

. `hash` 的处理，目前使用下面的方式从给定的 `ccPoint` 生成 `hash` 值作为 `Key` 
进行存储:

``` javascript
LoS.Helper.pointToHashCode = function(point) {
    return point.x + "_" + point.y;
};
```

字符串效率可能会比较差，尝试类似 `Java` 中的 `int hashCode` 方式。

### Quadtree / Spatial Hashing / 四叉树 / 空间散列

对于限制视线距离类型，进一步减少 `Culling` 阶段的候选比较对象数量

> [Quadtree vs Spatial Hashing - a Visualization](http://zufallsgenerator.github.io/2014/01/26/visually-comparing-algorithms/)
>
> [Spatial-hashing](https://www.gamedev.net/articles/programming/general-and-gameplay-programming/spatial-hashing-r2697/)
>
> [quadtree-js](https://github.com/timohausmann/quadtree-js/)

### Specific Obstacle Support / 特定障碍物对应

当前的算法对于障碍物采用了顶点数组的方式来描述，但是对于一些特殊形状，如 `圆形` `n阶贝塞尔曲线` 等，完全可以用更少的数据量来描述，甚至对射线算法部分的优化也有一定的帮助，起到**减少交点，减少射线，减少计算量**的效果。

### Bretter Intersection / 更好的相交算法

`LoS` 算法中一个很重要的环节就是判断相交，或是求交点的算法，因此对这个部分进行优化，找到更好更合适的算法，也是非常重要的。

> [Separating Axis Theorem (SAT) Explanation](http://www.sevenson.com.au/actionscript/sat/)
>
> [多边形碰撞检测 -- 分离轴算法](https://www.jianshu.com/p/4000a301c32a)

### Better Light Shader / 更好的光照 Shader

目前的 `Shader` 主要只是用来做大致的演示，效果还是有些问题的:

. 需要选择更合适的混合模式，目前特定颜色光照相互重叠情况下显示存在问题

. 光照效果的半径和实际视线范围半径并不是很匹配

### Better Camera / 更好的相机

这个主要是针对 `cocos2dx` 提出的。因为 `cocos2dx` 没有像 `Creator` 那样的相机功能，目前如果在大地图上开启 `LoS` 功能，当宿主开始移动的时候，移动地图时，性能也会受到一定的影响。这里可以参考 `Creator` 中配合 `Camera` 实现的地图移动方式来做一些优化。

### Real 3D / 3D 对应

随着 `CocosCreator 3D` 开始越来越成熟，自然也会想到把这个功能移植到 `3D` 环境下。

> [A light post](http://www.byte56.com/2011/06/a-light-post.html)

### More Refactor / 重构

. `Core` 轻量化

. 减少模块之间相互依赖

. 输入，输出，临时数据细分，归类，统一管理

### Gizmo Support / 编辑器可视化支持

这个在最早的 `Creator v1.5` 已经成功的添加了一些支持，只是暂时还没试过在 `Creator v2.x `是否仍然使适用。

### Better Demo

. `TiledMap` 

. `Boxed2d`

## Glossary / 术语表

. `endpoint` 端点

. `edge` 边

. `polygon` 多边形

. `sector` 扇形

. `Sight Distance` 视线距离

. `Source Position` 光源位置

. `Source Direction` 光源朝向

. `Sight Angle` 光源夹角

. `Raycasting` 光源发出射线

​    .. `Primary Ray` 主射线

​        从光源向角点发出的射线

​    .. `Auxiliary Ray` 辅助射线

​        最多两条，以光源为轴，将主射线顺时针，逆时针转动极小角度获得的两条射线，处理边形成的角落时使用

​    .. `Iota Angle` 极小角度

. `Corner` 拐角

​    同一个多边形的两条边形成的角，(唯一，因为所有边都是有向的)

​    .. `Exposed Conner`

​        暴露的拐角，不需要辅助射线

​    .. `Hidden Conner` 隐藏的拐角

​        可能需要一条或两条辅助射线

. `Angle Point` 角点

​    需要光源发射光线的目标端点，不是所有的端点都是角点

. `Implicit Angle Point` 

​    在限制视线夹角的情况下，会需要增加两个角点，扇形的两个边的远端点

. `Potential Blocking Edge` 潜在障碍边

​    可能阻挡视线继续前进的边

. `Blocking Edge` 障碍边

​    实际阻挡视线继续前进的边

. `Source Position` 光源位置

. `Hit Point` 碰撞点

​    视线与障碍物的交点

​    .. `Angle Hit Point` 角点碰撞点

​        碰撞点发生在角点上

​    .. `Edge Hit Point` 边碰撞点

​        碰撞点发生在边上

​    .. `Bounday Hit Point` 边界碰撞点

​        碰撞点发生在边界上

. `Sight Area` 可视区域

​    视线实际的可视区域

. `Culling` 剔除

​    尽量从 `edge` 中剔除与计算无关的对象的过程

​    .. `Broad Phase` 粗测阶段

​    .. `NarrowPhase Phase` 细测阶段

